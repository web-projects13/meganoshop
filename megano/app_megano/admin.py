from django.contrib import admin
from app_megano.models import Category,SubCategory,Role,User,Product,Cart,CartItems,Review,Order,OrderItem,Photo

# Register your models here.

admin.site.register(Category)
admin.site.register(SubCategory)
admin.site.register(Role)
admin.site.register(User)
admin.site.register(Product)
admin.site.register(Cart)
admin.site.register(CartItems)
admin.site.register(Review)
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(Photo)