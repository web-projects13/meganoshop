# Generated by Django 3.2.7 on 2022-01-20 18:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_megano', '0010_review'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='review',
            name='user',
        ),
        migrations.AddField(
            model_name='review',
            name='user_name',
            field=models.CharField(default='Гость', max_length=100, verbose_name='пользователь'),
        ),
    ]
