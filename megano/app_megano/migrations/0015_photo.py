# Generated by Django 3.2.7 on 2022-01-24 17:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app_megano', '0014_orderitem_count'),
    ]

    operations = [
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.FileField(blank=True, null=True, upload_to='product_photo/', verbose_name='фото продукта')),
                ('general', models.BooleanField(default=False, verbose_name='главное')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_megano.product', verbose_name='продукт')),
            ],
            options={
                'verbose_name': 'Photo',
                'verbose_name_plural': 'Photos',
                'db_table': 'Photo',
            },
        ),
    ]
