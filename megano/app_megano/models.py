from itertools import product
from pyexpat import model
from statistics import mode
from tkinter import CASCADE
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.fields import BooleanField, CharField
# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=500,verbose_name=("название"))
    icon=models.FileField(upload_to='product/',blank=True,null=True,verbose_name=("иконка"))
    

    class Meta():
        verbose_name_plural="categories"
        verbose_name = "category"
        db_table = "Category"

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    category = models.ManyToManyField(Category, verbose_name='Категория')
    name = models.CharField(max_length=25, unique=True, verbose_name='Название')
    icon=models.FileField(upload_to='icon/',blank=True,null=True,verbose_name=("иконка"))

    class Meta:
        verbose_name_plural = 'Subcategories'
        verbose_name = 'Subcategory'
        db_table = "SubCategory"

    def __str__(self):
        return self.name

class Role(models.Model):
    name = CharField(max_length=500,verbose_name="роль")

    class Meta:
        verbose_name_plural = 'Roles'
        verbose_name = 'Role'
        db_table = "Role"
         
    def __str__(self):
        return self.name


class User(AbstractUser):
    city = models.CharField(max_length=500,blank=True,null=True,verbose_name="город")
    age = models.IntegerField(blank=True,null=True,verbose_name="возраст")
    phone = models.CharField(max_length=12,blank=True,null=True,unique=True,verbose_name="телефон")
    balance = models.FloatField(default=0,verbose_name="баланс")
    role = models.ForeignKey(Role,on_delete=models.CASCADE,verbose_name="роль",blank=True,null=True)
    photo=models.FileField(upload_to='user_photo/',blank=True,null=True,verbose_name=("фото пользователя"))

    def __str__(self):
        return self.username

class Product(models.Model):
    name = models.CharField(max_length=500,verbose_name="наименования")
    description = models.CharField(max_length=5000,verbose_name="описание")
    category = models.ForeignKey(Category,on_delete=models.CASCADE,verbose_name="категория")
    count = models.IntegerField(default=0,verbose_name="колличество")
    price = models.FloatField(default=0,verbose_name="цена")
    limited = models.BooleanField(default=False,verbose_name="ограниченный тираж")


    class Meta:
        verbose_name_plural = 'Products'
        verbose_name = 'Product'
        db_table = "Product"

    def __str__(self):
        return self.name

class Photo(models.Model):
    product = models.ForeignKey(Product,on_delete=models.CASCADE,verbose_name="продукт")
    photo = models.FileField(upload_to='product_photo/',blank=True,null=True,verbose_name=("фото продукта"))
    general = models.BooleanField(default=False,verbose_name="главное")

    class Meta:
        verbose_name_plural = 'Photos'
        verbose_name = 'Photo'
        db_table = "Photo"

    def __str__(self):
        return f"Фото продукта {self.product}"

class Cart(models.Model):
    owner = models.OneToOneField(User,on_delete=models.CASCADE,verbose_name="id владелеца")
    
    class Meta:
        verbose_name_plural = 'Carts'
        verbose_name = 'Cart'
        db_table = "Cart"

    def __str__(self):
        return f'Козрина пользователя {self.owner}'

class CartItems(models.Model):
    cart = models.ForeignKey(Cart,on_delete=models.CASCADE,verbose_name="корзина")
    product_id = models.IntegerField(verbose_name="id_товарa")
    count = models.IntegerField(default=0,verbose_name= 'количество')
    data = models.DateField(auto_now=True,verbose_name="дата")

    class Meta:
        verbose_name_plural = 'CartItems'
        verbose_name = 'CartItem'
        db_table = "CartItem"

    def __str__(self):
        return f'{self.cart} - id товара {self.product_id }'

class Review(models.Model):
    product = models.ForeignKey(Product,on_delete=models.CASCADE,verbose_name="товар")
    user_name = models.CharField(max_length=100,default="Гость",verbose_name="пользователь")
    text = models.TextField(max_length=5000,verbose_name="текст")
    data = models.DateField(auto_now=True,verbose_name="дата")

    class Meta:
        verbose_name_plural = 'Reviews'
        verbose_name = 'Review'
        db_table = "Review"

    def __str__(self):
        return f"Отзыв пользователя {self.user_name} о товре {self.product}"

class Order(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE,blank=True,null=True,verbose_name="пользователь")
    data = models.DateField(auto_now=True,verbose_name="дата")
    price = models.IntegerField(default=0,verbose_name= 'цена')
    status = models.BooleanField(default=False,verbose_name='статус')

    class Meta:
        verbose_name_plural = 'Orders'
        verbose_name = 'Order'
        db_table = "Order"

    def __str__(self):
        return f"Заказ № {self.id} - пользователь {self.user}"

class OrderItem(models.Model):
    order = models.ForeignKey(Order,on_delete=models.CASCADE,verbose_name="заказ")
    product = models.ForeignKey(Product,on_delete=models.CASCADE,verbose_name="товар")
    count = models.IntegerField(default=0,verbose_name= 'количество')

    class Meta:
        verbose_name_plural = 'OrderItems'
        verbose_name = 'OrderItem'
        db_table = "OrderItem"

    def __str__(self):
        return f"{self.order} - товар {self.product}"