from django import forms
from django.contrib.auth.forms import UserCreationForm
from app_megano.models import  User


class RegisterUserForm(UserCreationForm):
    
    class Meta:
        model=User
        fields=["first_name","last_name","username",'age','phone','email','city','photo','password1','password2']