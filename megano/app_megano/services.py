from django.http import request
from app_megano.models import Cart,User,Product,CartItems,Review,Order,OrderItem



class Carts():

    def __init__(self,request):
        self.session = request.session
        if User.objects.filter(id=request.user.id).exists() == True:
            self.cart = Cart.objects.get(owner=User.objects.get(id=request.user.id))
        else:
            self.cart = self.session.get('cart')
            if not self.cart:
                self.cart= self.session['cart']={}

    def add_item_to_cart(self,product_id,coun): # Добавляет товар в корзину
        if type(self.cart) == dict:
            product_id= str(product_id)
            if product_id not in self.cart:
                self.cart[product_id]={
                    "count":coun
                }
            else:
                self.cart[product_id]["count"]+=coun
            self.session['cart'] = self.cart
            self.session.modified = True
        else:
            if CartItems.objects.filter(cart=self.cart,product_id=product_id).exists() == True:
                count = CartItems.objects.get(cart=self.cart,product_id=product_id).count
                CartItems.objects.filter(cart=self.cart,product_id=product_id).update(count=count+coun)
            else:
                CartItems.objects.create(cart=self.cart,product_id=product_id,count=coun)
        print(self.cart)
    
    def remove_item_to_cart(self,product_id): # Удаляет выбранный товар из корзины
        if type(self.cart) == dict:
            product_id= str(product_id)
            del self.cart[product_id]
            self.session['cart'] = self.cart
            self.session.modified = True
        else:
           CartItems.objects.filter(product_id=product_id).delete()

    def empty_trash(self): # Полностью очищает корзину
        if type(self.cart) == dict:
            self.cart=self.session['cart'] = {}
            self.session.modified = True
        else:
            CartItems.objects.filter(cart_id=self.cart.id).delete()
    
    def change_product_count(self,product_id,count): #Изменяем колличество товара в корзине
        if type(self.cart) == dict:
            product_id=str(product_id)
            self.cart[product_id]["count"]=count
            self.session['cart'] = self.cart
            self.session.modified = True
        else:
            CartItems.objects.filter(product_id=product_id).update(count=count)
    
    def get_cart_items(self): #Получаем список товаров в корзине (наименование,цена,количество)
        if type(self.cart) == dict:
           cart_items=self.cart
           for product_cart in self.cart:
               product = Product.objects.get(id=product_cart)
               cart_items[product_cart]['name']=product.name
               cart_items[product_cart]['price']=product.price
        else:
            items = CartItems.objects.filter(cart=self.cart)
            for product_cart in items:
                product = Product.objects.get(id=product_cart.product_id)
                cart_items = {
                    product_cart.product_id:{
                        'name':product.name,
                        'price': product.price,
                    }}
        print(cart_items)

class Reviews():

    def __init__(self,request):
        if request.user.is_authenticated:
            self.user = request.user.username
        else:
            self.user = 'Guest'
        print(self.user)
    
    def add_review(self,product,text):
        Review.objects.create(
            product=product,
            user_name = self.user,
            text = text
            )
    
    def get_review(self,product):
        reviews_list=Review.objects.filter(product=product)
        return reviews_list

    def get_sale(self,username,sale):
        if Review.objects.filter(user_name=username).exists:
            return int(sale)


class Buy():
    def __init__(self,request):
        self.request = request

    def pay_order(self,order): # Оплата заказа
        order = Order.objects.get(id=order)
        price = order.price
        order_item = OrderItem.objects.select_related('order').filter(order=order)
        for product_order in order_item:
            if product_order.product.count>=product_order.count :
                new_count = product_order.product.count - product_order.count
                product_order.product.count = product_order.product.count - product_order.count
                product_order.product.save()
            else:
                raise f"Товар  {product_order} отсутствует"
        payment = True
        if payment == True:
            order.status=True
            order.save()
        else:
            raise "Оплата не прошла"
        