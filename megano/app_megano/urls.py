from os import name
from django.urls import path,include
from django.conf.urls import url
from . import views

urlpatterns = [
    path('megano',views.Main.as_view(),name = "main"),
    path('megano/authorization',views.Login.as_view(),name="authorization"),
    path('megano/logout',views.UserLogoutView.as_view(),name="logout"),
    path('megano/registration',views.RegisterUserView.as_view(),name="registration"),
    path('megana/catalog',views.CatalogView.as_view(),name='catalog'),
    url(r'', views.CatalogView.as_view(),name='catalog_filter')
]