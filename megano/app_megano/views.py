
from re import template
from tkinter import Widget
from django import forms
from django.shortcuts import render
from django.views.generic.base import View
from django.views.generic import ListView
from django.contrib.auth.views import LoginView,LogoutView
from django.urls.base import reverse_lazy
from django.contrib.auth import authenticate, login
from django.http.response import HttpResponseRedirect
from  django.views.generic.edit import FormView
from app_megano.models import User,Role,Product
from app_megano.forms import RegisterUserForm
from app_megano.services import Carts,Reviews,Buy
import django_filters



class Main(View):
    def get(self,request):
        cart = Buy(request)
        cart.pay_order(1)
        return render(request,'app_megano/index.html',{})

class Login(LoginView):
    template_name= 'app_megano/login.html'

    def form_valid(self, form):
        user = form.get_user()
        login(self.request,user)
        return HttpResponseRedirect(self.get_success_url())

class UserLogoutView(LogoutView):
    next_page=reverse_lazy('main')


class RegisterUserView(FormView):
    form_class = RegisterUserForm
    template_name = 'app_megano/register.html'
    success_url = reverse_lazy("main")

    def form_valid(self, form):
        user = form.save(commit=False)

        user.role = Role.objects.get(id=2)
        form.save()
        username = form.cleaned_data['username']
        password=form.cleaned_data['password1']
        user=authenticate(username=username,password=password)
        login(self.request,user)
        return HttpResponseRedirect(self.get_success_url())



class ProductFilter(django_filters.FilterSet):
    price = django_filters.RangeFilter(widget=forms.widgets.TextInput(attrs={'class': 'myfieldclass'}))
    
    class Meta:
        model = Product
        fields = ["price"]

class CatalogView(ListView):
    model = Product
    template_name = "app_megano/catalog.html"
    filter_class = ProductFilter

    def get_queryset(self):
        queryset = super().get_queryset()
        self.f = self.filter_class(self.request.GET, queryset=queryset)
        return self.f.qs
    
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['products'] = Product.objects.all()
        context['filter'] = self.f
        return context
